import csv

def read_in_csv(filename):
    data = []
    with open(filename, "rU") as csvfile:
        reader = csv.reader(csvfile, dialect='excel')
        header = next(reader)
        print "Categories: ", header[1:]
        for line in reader:
            line = [fixRestaurantNames(x) for x in line]
            data.append(line)
    return data

def write_csv(data, filename):
    with open(filename, 'wb') as csvfile:
        writer = csv.writer(csvfile, dialect = 'excel')
        writer.writerows(data)


def fixRestaurantNames (name):
	new_name = ""
	for i in name:
		if i == "_":
			new_name += " "
		else:
			new_name += i
	return new_name

