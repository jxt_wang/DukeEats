#!/usr/bin/env python

from flask import Flask, render_template, request
import sql_all as sql

app = Flask(__name__)

@app.route("/")
def index():
  return render_template("main.html")

@app.route("/about")
def about():
  return render_template("about.html")

@app.route("/Search")
def Search():
  return render_template("Search.html")

@app.route("/restaurants")
def restuarants():
  return render_template("restaurants.html")

@app.route("/signup", methods=['GET', 'POST'])
def login():
  name = None

  if request.method == 'POST':
    name = str(request.form['email'])
    sql.insertUser(name)

  return render_template("signup.html", name = name)


@app.route("/restaurant_profile", methods=['GET', 'POST'])
def restaurant_profile():
    if request.method == 'POST':
        restaurant = str(request.form["restaurant"])                         #get the restaurant name
        rows = sql.selectAll(2, restaurant_name=restaurant)                  #Query information from restaurant
        profile_img = "static/img/RestaurantPhoto/" + restaurant + ".jpg"    #url for profile image in restuarnt_profile.html
        posts = [dict(name=row[0],
                      open_time=row[1],
             	      close_time=row[2],
                      location=row[3],
                      cuisine=row[4],
                      sat_op_time=row[5],
                      sat_close_time=row[6],
             	      sun_op_time=row[7],
             	      sun_close_time=row[8],
                      image=profile_img) for row in rows]                   #should get only 1 row.
        return render_template("restaurant_profile.html", posts=posts)



@app.route("/display", methods=['GET','POST'])
def display():                                                       #display the result of either search or Restaraunt Search
  prev_url = request.referrer
  print(prev_url)
  if request.method == 'POST':
      posts = []
      rows = []

      if prev_url == "http://127.0.0.1:5000/Search" or prev_url == "http://127.0.0.1:5000/display":
            cuisine= str(request.form['cuisine'])
            rating= str(request.form['rating'])
            location= str(request.form['location'])
            price= (str(request.form['price'])).split("-")            #split the price and set the lower, uppder bound
            lower = price[0]                                          #Ex: "5-10" => Lower=5, Upper=10
            upper = price[1]
            rows = sql.selectAll(0, cuisine, location, lower, upper)

            for row in rows:
                actual_rating = sql.getAvgItemRating(rows[0][3],rows[0][0])[0]
                addon = []
                if actual_rating >= int(rating):                      # filter based on rating preferences
                    addon_list = sql.selectAll(1, item_name=row[0], restaurant_name=row[3])
                    for addon_row in addon_list:
                        addon.append(dict(addon_name=addon_row[0], addon_price=addon_row[1]))
                    posts.append(dict(name=row[0],
                                      price=row[1],
                                      is_side=row[2],
                                      restaurant=row[3],
                                      location=row[4],
                                      cuisine=row[5],
                                      rating=round(actual_rating,2),
                                      addons=addon))


      elif prev_url == "http://127.0.0.1:5000/restaurant_profile":        # when getting information of
            restaurant = str(request.form['restaurant'])                    # ALL menues in the restaurant
            rows = sql.selectAll(7, restaurant_name=restaurant)
            actual_rating = sql.getAvgItemRating(rows[0][3],rows[0][0])[0]
            
            for row in rows:
                addon = []
                addon_list = sql.selectAll(1, item_name=row[0], restaurant_name=row[3])
                for addon_row in addon_list:
                    addon.append(dict(addon_name=addon_row[0], addon_price=addon_row[1]))
                posts.append(dict(name=row[0],
                                  price=row[1],
                                  is_side=row[2],
                                  restaurant=row[3],
                                  location=row[4],
                                  cuisine=row[5],
                                  rating=round(actual_rating,2),
                                  addons=addon))

      if len(posts) == 0:                           # show the user that there is no menu satisfying user's preferences
            posts.append(dict(name="",
                              price="",
                              is_side="",
                              restaurant="No Result Found",
                              location="",
                              cuisine="",
                              rating="",
                              addon=""))


      return render_template("display.html", posts=posts)


if __name__ == "__main__":
  app.run(debug=True)
