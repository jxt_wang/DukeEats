DROP TABLE IF EXISTS restaurants;

CREATE TABLE restaurants (
    name TEXT NOT NULL PRIMARY KEY,
	open_time REAL NOT NULL,
	close_time REAL NOT NULL,
    location TEXT NOT NULL,
    cuisine TEXT NOT NULL,
    sat_op_time REAL NOT NULL,
    sat_close_time REAL NOT NULL,
	sun_op_time REAL NOT NULL,
	sun_close_time REAL NOT NULL
);

DROP TABLE IF EXISTS menu;

CREATE TABLE menu (
    name TEXT NOT NULL,
	price REAL NOT NULL,
	is_side INTEGER NOT NULL,
	restaurant TEXT NOT NULL REFERENCES restaurants(name),
	location TEXT NOT NULL REFERENCES restaurants(location),
	cuisine TEXT NOT NULL,
	PRIMARY KEY (name, restaurant)
);

DROP TABLE IF EXISTS addons;

CREATE TABLE addons (
    name TEXT NOT NULL,
	price REAL NOT NULL,
	item TEXT NOT NULL REFERENCES menu(name),
	restaurant TEXT NOT NULL REFERENCES restaurants(name),
	PRIMARY KEY (name, item, restaurant)
);

DROP TABLE IF EXISTS user;

CREATE TABLE user (
	name TEXT NOT NULL PRIMARY KEY
);

DROP TABLE IF EXISTS rates_restaurant;

CREATE TABLE rates_restaurant (
	restaurant TEXT NOT NULL REFERENCES restaurants(name),
	user TEXT NOT NULL REFERENCES user(name),
	rating INTEGER,
	PRIMARY KEY (restaurant, user)
);

DROP TABLE IF EXISTS rates_item;

CREATE TABLE rates_item (
	restaurant TEXT NOT NULL REFERENCES restaurants(name),
	item TEXT NOT NULL REFERENCES menu(name),
	user TEXT NOT NULL REFERENCES user(name),
	rating INTEGER,
	PRIMARY KEY (restaurant, item, user)
);

DROP TRIGGER IF EXISTS new_user_rating;

CREATE TRIGGER new_user_rating AFTER INSERT ON user
	BEGIN
		INSERT INTO rates_restaurant (restaurant, user, rating)
		SELECT restaurants.name, new.name, NULL
		FROM restaurants;

		INSERT INTO rates_item (restaurant, item, user, rating)
		SELECT menu.restaurant, menu.name, new.name, NULL
		FROM menu;
	END;

DROP TRIGGER IF EXISTS update_user_rating;

CREATE TRIGGER update_user_rating AFTER UPDATE ON user
	BEGIN
		UPDATE rates_restaurant 
		SET user = new.name
		WHERE user = old.name;

		UPDATE rates_item
		SET user = new.name
		WHERE user = old.name;
	END;

DROP TRIGGER IF EXISTS delete_user_rating;

CREATE TRIGGER delete_user_rating AFTER DELETE ON user
	BEGIN
		DELETE FROM rates_restaurant
		WHERE user = old.name;

		DELETE FROM rates_item
		WHERE user = old.name;
	END; 
