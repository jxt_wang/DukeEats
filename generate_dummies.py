import csv
import names 
import random
from sklearn.externals import joblib

def generate_users(filename, num_users = 5000):
	domains = ["hotmail.com", "gmail.com", "aol.com", "outlook.com", "yahoo.com", "mail.com", "comcast.net", "icloud.com", "inbox.com"]
	users = []
	for i in range(num_users):
		new_user = names.get_full_name()
		name_split = new_user.split(" ")
		new_user = name_split[0] + "." +  name_split[1] + "@" + get_random(domains)
		if new_user not in users: 
			users.append(new_user)
	print len(users)
	with open(filename, 'wb') as csvfile:
		writer = csv.writer(csvfile, dialect = 'excel')
		writer.writerow(["name"])
		for user in users:
			writer.writerow([user])
	joblib.dump(users, "users.pkl")


def generate_restaurant_ratings(filename, num_ratings = 2000):
	users = joblib.load("users.pkl")
	ratings = [1,2,3,4,5]
	restaurants = []
	all_ratings = []
	with open("Restaurants_final.csv", 'rU') as csvf:
		reader = csv.reader(csvf, dialect = 'excel')
		header = reader.next()
		for line in reader:
			restaurants.append(line[0])
	joblib.dump(restaurants, "restaurants.pkl")
	with open(filename, 'wb') as csvfile:
		writer = csv.writer(csvfile, dialect = 'excel')
		writer.writerow(["restaurant", "user", "rating"])
		for i in range(num_ratings):
			new_rating = []
			new_rating.append(get_random(restaurants))
			new_rating.append(get_random(users))
			new_rating.append(get_random(ratings))
			if new_rating not in all_ratings:
				writer.writerow(new_rating)


def generate_item_ratings(filename, num_ratings = 2000):
	users = joblib.load("users.pkl")
	ratings = [1,2,3,4,5]
	menu_items = []
	all_ratings = []
	with open("Menu_final.csv", 'rU') as csvf:
		reader = csv.reader(csvf, dialect = 'excel')
		header = reader.next()
		for line in reader:
			tmp = (line[3], line[0])
			menu_items.append(tmp)
	joblib.dump(menu_items, "menu.pkl")
	with open(filename, 'wb') as csvfile:
		writer = csv.writer(csvfile, dialect = 'excel')
		writer.writerow(["restaurant", "item", "user", "rating"])
		for i in range(num_ratings):
			new_rating = []
			tmp = get_random(menu_items)
			new_rating.append(tmp[0])
			new_rating.append(tmp[1])
			new_rating.append(get_random(users))
			new_rating.append(get_random(ratings))
			if new_rating not in all_ratings:
				writer.writerow(new_rating)
			


def get_random(list):
	return random.choice(list)


if __name__ == "__main__":
	generate_users("users.csv", 1000)
	generate_restaurant_ratings("restaurant_ratings.csv")
	generate_item_ratings("item_ratings.csv")


