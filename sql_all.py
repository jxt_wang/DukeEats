import sqlite3 as sql
import csv

def parseMenu(filename):
	f = open(filename, "rU")
	list_row = []
	reader = csv.reader(f)
	first_row = True
	for info in reader:
		if first_row:
			first_row = False
		else:
			name = str(info[0])
			price = float(info[1])
			is_side = str(info[2]).startswith('y')
			restaurant = str(info[3])
			location = str(info[4])
			cuisine = str(info[5])
			list_row.append((name,price,is_side,restaurant, location, cuisine))
	f.close()
	return list_row


def convertTimes(time):
	if 'AM' in time:
		colon_index = time.index(':')
		hours = float(int(time[0:colon_index]) % 12)
		minutes = float(time[colon_index + 1:colon_index + 3])/100.0
		return hours + minutes
	elif 'PM' in time:
		colon_index = time.index(':')
		hours = float(int(time[0:colon_index]) % 12) + 12.0
		minutes = float(time[colon_index + 1:colon_index + 3])/100.0
		return hours + minutes
	return -1.0

def parseRestaurants(filename):
	f = open(filename, "rU")
	list_row = []
	reader = csv.reader(f)
	first_row = True
	for info in reader:
		if first_row:
			first_row = False
		else:
			name = str(info[0])
			open_time = (str(info[1]))
			close_time = (str(info[2]))
			location = str(info[3])
			cuisine = str(info[4])
			sat_op_time = (str(info[5]))
			sat_close_time = (str(info[6]))
			sun_op_time = (str(info[7]))
			sun_close_time = (str(info[8]))
			list_row.append((name,open_time,close_time,location,cuisine,
								sat_op_time,sat_close_time,sun_op_time,
								sun_close_time))
	f.close()
	return list_row

def parseAddons(filename):
	f = open(filename, "rU")
	list_row = []
	reader = csv.reader(f)
	first_row = True
	for info in reader:
		if first_row:
			first_row = False
		else:
			name = str(info[0])
			price = float(info[1])
			item = str(info[2])
			restaurant = str(info[3])
			list_row.append((name,price,item,restaurant))
	f.close()
	return list_row


def parseRestaurantRatings(filename):
	f = open(filename, "rU")
	list_row = []
	reader = csv.reader(f)
	first_row = True
	for info in reader:
		if first_row:
			first_row = False
		else:
			restaurant = str(info[0])
			user = str(info[1])
			rating = float(info[2])
			list_row.append((restaurant,user,rating))
	f.close()
	return list_row

def parseUsers(filename):
	f = open(filename, "rU")
	list_row = []
	reader = csv.reader(f)
	first_row = True
	for info in reader:
		if first_row:
			first_row = False
		else:
			email = str(info[0])
			list_row.append((email,))
	f.close()
	return list_row

def parseItemRatings(filename):
	f = open(filename, "rU")
	list_row = []
	reader = csv.reader(f)
	first_row = True
	for info in reader:
		if first_row:
			first_row = False
		else:
			restaurant = str(info[0])
			item = str(info[1])
			user = str(info[2])
			rating = float(info[3])
			list_row.append((restaurant,item, user,rating))
	f.close()
	return list_row



def insertAll(list_row,table):
	con = sql.connect('database.db')
	cur = con.cursor()

	if table == 0:
		query = ('INSERT INTO menu (name,price,is_side,restaurant,location,cuisine) ' +
				'VALUES (?,?,?,?,?,?)')
	elif table == 1:
		query = ('INSERT INTO addons (name,price,item,restaurant) ' +
				'VALUES (?,?,?,?)')
	elif table == 2:
		query = ('INSERT INTO restaurants ' +
				'(name,open_time,close_time,location,cuisine,sat_op_time,' +
				'sat_close_time,sun_op_time,sun_close_time) ' +
				'VALUES (?,?,?,?,?,?,?,?,?)')
	elif table == 3:
		query = ('INSERT INTO user (name) ' +
				'VALUES (?)')
	elif table == 4:
		query = ('INSERT INTO rates_restaurant (restaurant, user, rating) ' +
				'VALUES (?, ?, ?)')
	elif table == 5:
		query = ('INSERT INTO rates_item (restaurant, item, user, rating) ' +
				'VALUES (?, ?, ?, ?)')

	for row in list_row:
		cur.execute(query,row)

	con.commit()
	con.close()

"""
selectAll(0) = menu items
selectAll(1) = addon items
selectAll(2) = restaurant items
selectAll(3) = users
selectAll(4) = restaurant ratings
selectAll(5) = item ratings
"""

'''
This part was modified by Brian Hur.
'''

def selectAll(table, cuisine=None, location=None, lower_price=None, upper_price=None, restaurant_name=None, item_name=None):
	con = sql.connect('database.db')
	cur = con.cursor()

	if table == 0:
		query = 'SELECT * FROM menu WHERE cuisine = ' + cuisine + ' AND location = ' + location + ' AND price >= ' + lower_price + " AND price < " + upper_price
		cur.execute(query)

	elif table == 1:
		cur.execute('SELECT * FROM addons WHERE (item = ? OR item = "any")  AND restaurant = ?', [item_name, restaurant_name])

	elif table == 2:
		cur.execute('SELECT * FROM restaurants WHERE name = ?', [restaurant_name])
	# elif table == 3:
	# 	query = 'SELECT * FROM user'
	# elif table == 4:
	# 	query = 'SELECT * FROM rates_restaurant'
	# elif table == 5:
	# 	query = 'SELECT * FROM rates_item'
	elif table == 7:
		cur.execute('SELECT * FROM menu WHERE restaurant = ?', [restaurant_name])

	# cur.execute(query)
	rows = cur.fetchall()
	return rows

def insertUser(name):
	con = sql.connect('database.db')
	cur = con.cursor()

	cur.execute('INSERT INTO user (name) VALUES(?)', (name,))

	con.commit()
	con.close()

def updateRestaurantRating(restaurant, user, rating):
	con = sql.connect('database.db')
	cur = con.cursor()

	cur.execute('UPDATE rates_restaurant SET rating = ? WHERE restaurant = ? AND user = ?',
				(rating, restaurant, user))

	con.commit()
	con.close()

def updateItemRating(restaurant, item, user, rating):
	con = sql.connect('database.db')
	cur = con.cursor()

	cur.execute('UPDATE rates_item SET rating = ? WHERE restaurant = ? AND item = ? AND user = ?',
				(rating, restaurant, item, user))

	con.commit()
	con.close()

def getAvgRestaurantRating(restaurant):
	con = sql.connect('database.db')
	cur = con.cursor()

	cur.execute('SELECT AVG(rating) FROM rates_restaurant WHERE restaurant = ?',
				(restaurant))
	return cur.fetchone()

def getAvgItemRating(restaurant, item):
	con = sql.connect('database.db')
	cur = con.cursor()

	cur.execute('SELECT AVG(rating) FROM rates_item WHERE restaurant = ? AND item = ?',
				(restaurant, item))
	return cur.fetchone()

if __name__ == "__main__":
	menu = parseMenu("Menu_final.csv")
	insertAll(menu, 0)

	addons = parseAddons("Addons_final.csv")
	insertAll(addons, 1)

	restaurants = parseRestaurants("Restaurants_final.csv")
	insertAll(restaurants, 2)

	users = parseUsers("users.csv")
	insertAll(users, 3)

	restaurant_ratings = parseRestaurantRatings("restaurant_ratings.csv")
	insertAll(restaurant_ratings, 4)

	item_ratings = parseItemRatings("item_ratings.csv")
	insertAll(item_ratings, 5)
	
